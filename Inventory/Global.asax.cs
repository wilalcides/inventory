﻿using Inventory.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Inventory
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.InventoryContext, Migrations.Configuration>());
            this.Validarsuperusuario();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
			
        }

        private void Validarsuperusuario()
        {
			var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var db = new InventoryContext();

            
            this.ValidarRol("Administrador", userContext);
            this.ValidarRol("Usuario", userContext);
			
		
			//se valida y se garantiza que exista una sede
			var sede = db.Sedes.Where(s => s.Nombre.ToLower().Equals("Campus Robledo")).FirstOrDefault();

			if (sede == null)
			{

				sede = new Sede
				{
					Nombre = "Campus Robledo",
					Direccion = "Calle 73 No. 76A - 354, Vía al Volador",
					Telefono = "440 51 00",
                    Estado = "Activa"
				};

				db.Sedes.Add(sede);
				db.SaveChanges();

			}

			//se valida y se garantiza que exista unCentro de costos
			var centrocosto = db.Centrodecostos.Where(c => c.Nombre.ToLower().Equals("ROB0000001")).FirstOrDefault();

            if (centrocosto == null)
            {

                centrocosto = new Centrodecosto
                {
                    Nombre = "ROB0000001",
                    Estado = "Activo",
					SedeId = 1,
                };

                db.Centrodecostos.Add(centrocosto);
                db.SaveChanges();

            }

            

            //se valida y se garantiza que exista un laboratorio
            var laboratorio = db.Laboratorios.Where(l => l.Nombre.ToLower().Equals("Centro de laboratorios")).FirstOrDefault();

            if (laboratorio == null)
            {

                laboratorio = new Laboratorio
                {
                    Nombre = "Centro de laboratorios",
                    Telefono = "440 51 00",
                    Estado = "Activo",
                    SedeId = 1,
                    CentrodecostoId = 1


                };

                db.Laboratorios.Add(laboratorio);
                db.SaveChanges();

            }


            //se valida y se garantiza que existan los tipos de historicos 
            var tipo = db.Tipo_Historicos.Where(e => e.Nombre.ToLower().Equals("Mantenimiento")).FirstOrDefault();

            if (tipo == null)
            {

                tipo = new Tipo_Historico
                {
                    Nombre = "Mantenimiento"
                };
                db.Tipo_Historicos.Add(tipo);
                tipo = new Tipo_Historico
                {
                    Nombre = "Seguimiento"
                };
                db.Tipo_Historicos.Add(tipo);

                db.SaveChanges();

            }




            //se valida y se garantiza que exista un estado 

            var estado = db.Estados.Where(e => e.Nombre.ToLower().Equals("Activo")).FirstOrDefault();

            if (estado == null)
            {

                estado = new Estado
                {
                    Nombre = "Activo"                    
                };
                db.Estados.Add(estado);
                estado = new Estado
                {
                    Nombre = "Inactivo"
                };
                db.Estados.Add(estado);
                estado = new Estado
                {
                    Nombre = "Mantenimiento"
                };
                db.Estados.Add(estado);
                estado = new Estado
                {
                    Nombre = "Dado de baja"
                };
                db.Estados.Add(estado);
                db.SaveChanges();

            }

            var usuario = db.Usuarios
                .Where(u => u.Logueo.ToLower().Equals("itminventory@gmail.com"))
                .FirstOrDefault();

            Usuario usuariocrear = new Usuario();

            if (usuario == null)
            {
                


                usuariocrear.Nombre = "Administrador";
                usuariocrear.Apellidos = "ITM";
                usuariocrear.Documento = "1111111";
                usuariocrear.Direccion = "itm";
                usuariocrear.Telefono = "11111111";
                usuariocrear.Cargo = "Administrador";
                usuariocrear.Logueo = "itminventory@gmail.com";
                usuariocrear.Valediasp = "itminventory@gmail.com";
                usuariocrear.Rol = "true";
                usuariocrear.Estado = "Activo";
                usuariocrear.LaboratorioId = 1;
                usuariocrear.SedeId = 1;
                usuariocrear.Contraseña = "Inventory2020.";
                usuariocrear.Valediasp = " ";
                usuariocrear.Confircontraseña = "Inventory2020.";
                usuariocrear.CodigoRecuperarContraseña = " ";
                usuariocrear.CodigoActivacion = Guid.NewGuid();
                usuariocrear.VerificacionEmail = false;

                db.Usuarios.Add(usuariocrear);
                db.SaveChanges();

           
           

                var userASP = userManager.FindByName(usuariocrear.Logueo);

                if (userASP == null)
                {
                    userASP = new ApplicationUser
                    {
                        UserName = usuariocrear.Logueo,
                        Email = usuariocrear.Logueo,
                        PhoneNumber = usuariocrear.Telefono,

                    };

                    userManager.Create(userASP, usuariocrear.Contraseña);

                }

                userManager.AddToRole(userASP.Id, "Administrador");
            }
        }


        private void ValidarRol(string roleName, ApplicationDbContext userContext)
        {
            
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }
        }
    }
}
