﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    [NotMapped]
    public class ReporteModelsVista
    {
        public List<ReporteModelsVista> Reportedisp { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha desde")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

       
        public DateTime FechaInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha hasta")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaFinal { get; set; }


        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un Estado ")]
        [Display(Name = "Estado")]
        public int EstadoId { get; set; }

        [Display(Name = "Estado")]
        public string Estatus { get; set; }

        public string Identificador { get; set; }

        [Display(Name = "Dispositivo")]
        public string Nombre { get; set; }

        [Display(Name = "Marca")]
        public string Marca { get; set; }

        [Display(Name = "Modelo")]
        public string Modelo { get; set; }

        [Display(Name = "Precio")]
        public double Precio { get; set; }

        [Display(Name = "Serial")]
        public string Serial { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de compra")]
        public DateTime Fecha { get; set; }

        public string Sede { get; set; }

        public string Laboratorio { get; set; }



    }
}