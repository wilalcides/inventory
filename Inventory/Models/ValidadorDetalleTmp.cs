﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class ValidadorDetalleTmp
    {

        public List<ValidadorDetalleTmp> ValitTmp { get; set; }




        [Key]
        public int ValidadorDetalleTmpId { get; set; }

        public int DispositivoId { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        public int UsuarioId { get; set; }



      

        public virtual Validador Validador { get; set; }
    }
}