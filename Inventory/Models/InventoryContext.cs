﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class InventoryContext :DbContext
	{
		public InventoryContext() : base ("DefaultConnection")
		{

		}

		public DbSet<Sede> Sedes { get; set; }

		public DbSet<Laboratorio> Laboratorios { get; set; }

		public DbSet<Historico> Historicos { get; set; }

		public DbSet<Dispositivo> Dispositivos { get; set; }

		public DbSet<Estado> Estados { get; set; }

		public DbSet<Centrodecosto> Centrodecostos { get; set; }

        public DbSet<Eventos> Eventos { get; set; }

        public DbSet<Tipo_Historico> Tipo_Historicos { get; set; }

        public DbSet<Reporte> Reportes { get; set; }

        public DbSet<Validador> Validaciones { get; set; }

        public DbSet<ValidadorDetalle> ValidadorDetalles { get; set; }

        public DbSet<ValidadorDetalleTmp> ValidadorDetalleTmps { get; set; }

        public DbSet<ReporteValidacion> ReporteValidaciones { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }






        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
		}

        
    }
}