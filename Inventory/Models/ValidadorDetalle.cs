﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class ValidadorDetalle
    {

        [Key]
        public int ValidadorDetalleId { get; set; }

        public int ValidadorId { get; set; }

        public int DispositivoId { get; set; }

        public int UsuarioId { get; set; }


       

        //public virtual ICollection<Dispositivo> Dispositivos { get; set; }

        public virtual Validador Validador { get; set; }

        public virtual Dispositivo Dispositivo { get; set; }

        //public virtual ReporteValidacion ReporteValidacion { get; set; }
       // public virtual ICollection<ReporteValidacion> ReporteValidacions { get; set; }

        


    }
}