﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Tipo_Historico
    {

        [Key]
        public int TipoId { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Estado_Nombre_Index", IsUnique = true)]
        public string Nombre { get; set; }




        public virtual ICollection<Historico> Historicos { get; set; }
    }
}