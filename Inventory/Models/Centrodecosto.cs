﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class Centrodecosto
	{

		[Key]
		public int CentrodecostoId { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Centro de costo")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Laboratorio_Nombre_Index", IsUnique = true)]
        public String Nombre { get; set; }

		
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar una Sede ")]
		[Display(Name = "Sede")]
		public int SedeId { get; set; }



		public virtual Sede Sede { get; set; }

        public virtual ICollection<Laboratorio> Laboratorios { get; set; }

    }
}