﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{

    [NotMapped]
    public class Restablecercontraseña
    {


        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Correo electronico:")]
        public string RestablecerContraseña { get; set; }



    }
}