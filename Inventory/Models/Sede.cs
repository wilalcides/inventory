﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class Sede
	{
	
		
		[Key]
		public int SedeId { get; set; }

        //[RegularExpression("^[A-ZÑ][a-zàèìòùñ]+$", ErrorMessage = "El nombre debe tener la primer letra en mayuscula")]
        [Display(Name = "Sede")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Sede_Nombre_Index",  IsUnique = true)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Direccion")]
		public string Direccion { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Telefono")]
		public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado")]
        public string Estado { get; set; }



        public virtual ICollection<Dispositivo> Dispositivos { get; set; }

        public virtual ICollection<Laboratorio> Laboratorios { get; set; }

		public virtual ICollection<Centrodecosto> Centrodecostos { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }


    }
}