﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class ModelConfirmarView
    {

        public int ValidadorDetalleId { get; set; }

        public int ValidadorId { get; set; }

        public int DispositivoId { get; set; }

        public int UsuarioId { get; set; }




        public string Identificador { get; set; }

        public string Dispositivo { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }
               

        
    }
}