﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Validador
    {
        [Key]
        public int ValidadorId { get; set; }

     
        public DateTime Fecha { get; set; } 

        public int UsuarioId { get; set; }

        public int SedeId { get; set; }

        public int LaboratorioId { get; set; }

        public int CentrodecostoId { get; set; }


       


        [DataType(DataType.ImageUrl)]
        public string Firmausuario1 { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Firmausuario2 { get; set; }

        [NotMapped]
        public HttpPostedFileBase Firmausuario1Imagen { get; set; }

        [NotMapped]
        public HttpPostedFileBase Firmausuario2Imagen { get; set; }




        public virtual ICollection<ValidadorDetalle> ValidadorDetalles { get; set; }

        public virtual ICollection<ValidadorDetalleTmp> ValidadorDetalleTmps { get; set; }

    }
}