﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class Laboratorio
	{
		[Key]
		public int LaboratorioId { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display (Name = "Laboratorio")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Laboratorio_Nombre_Index", 2, IsUnique = true)]
        public string Nombre { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Telefono")]
		public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado")]
        //[MaxLength(10, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]                  
        //[Index("Laboratorio_Estado_Index", 2, IsUnique = true)]
        public string Estado { get; set; }


        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar una Sede ")]
        [Display(Name = "Sede")]
        [Index("Laboratorio_Nombre_Index", 1, IsUnique = true)]
        public int SedeId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un centro de costos ")]
        [Display(Name = "Centro de costos")]
        public int CentrodecostoId { get; set; }





        public virtual Sede Sede { get; set; }

        public virtual Centrodecosto Centrodecosto { get; set; }


        public virtual ICollection<Dispositivo> Dispositivos { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }

       




    }
}