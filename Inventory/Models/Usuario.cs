﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Usuario
    {
        [Key]
        public int Usuarioid { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Correo electronico:")]
        [MaxLength(256, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Usuario_Nombre_Index", 1, IsUnique = true)]
        public string Logueo { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Nombres:")]
        
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Apellidos:")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Documento:")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        //para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Usuario_Documento_Index", 2, IsUnique = true)]
        public string Documento { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Direccion:")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Telefono:")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Cargo:")]
        public String Cargo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Rol")]
        public string Rol { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        public string Valediasp { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Sede:")]
        public int SedeId { get; set; }


        
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "La contraseña debe contener mas de 6 carasteres")]
        public string Contraseña { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Contraseña", ErrorMessage ="Las contraseñas ingresadas no coinsiden, ingreselas nuevamente")]
        public string Confircontraseña { get; set; }

        public string CodigoRecuperarContraseña { get; set; }


        public System.Guid CodigoActivacion { get; set; }

        public bool VerificacionEmail { get; set; }

        

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0} ")]
		[Display(Name = "Laboratorio:")]
        public int LaboratorioId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        //[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un estado ")]
        //[Display(Name = "Estado")]
        //      public int EstadoId { get; set; }



        public virtual ICollection<Eventos> Eventos { get; set; }

        public virtual ICollection<Historico> Historicos { get; set; }


        public virtual Laboratorio Laboratorio { get; set; }

        public virtual Sede Sede { get; set; }


    }
}