﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class Dispositivo
	{
		[Key]
		public int DispositivoId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Display(Name = "Identificador")]
        [Index("Dispositivo_Identificador_Index", IsUnique = true)]
        public string Identificador { get; set; }



        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Display(Name = "Dispositivo")]
       
        public string Nombre { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Marca")]
		public string Marca { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Modelo")]
		public string Modelo { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Precio")]
		public string Precio { get; set; }




		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Serial")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        // para aplicar el data notation Index se debe aplicar tambien el data notation
        //MaxLength
        [Index("Dispositivo_Serial_Index", IsUnique = true)]
        public string Serial { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[DataType(DataType.Date)]
		[Display(Name = "Fecha de compra")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar una sede ")]
        [Display(Name = "Sede")]
        
        public int SedeId { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un Laboratorio ")]
		[Display(Name = "Laboratorio")]
        
        public int LaboratorioId { get; set; }


        public string EstadoInventario { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un Estado ")]
		[Display(Name = "Estado")]
		public int EstadoId { get; set; }


        //public virtual Reporte Reporte { get; set; }

        public virtual Laboratorio Laboratorio { get; set; }
		
		public virtual Estado Estado { get; set; }

        public virtual Sede Sede { get; set; }

        //public virtual ValidadorDetalle ValidadorDetalle { get; set; }

        public virtual ICollection<Historico> Historicos { get; set; }
        public virtual ICollection<ValidadorDetalle> ValidadorDetalles { get; set; }



    }
}