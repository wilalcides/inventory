﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class Historico
	{

		[Key]
		public int HistoricoId { get; set; }

		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Tipo")]
		public int TipoId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [DataType(DataType.MultilineText)]
		[Required(ErrorMessage = "El campo {0} es obligatorio")]
		[Display(Name = "Observaciones")]
		public string Observaciones { get; set; }

		//[Display(Name = "Dispositivo")]
		//[Required(ErrorMessage = "El campo {0} es obligatorio")]
		//[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un Dispositivo ")]
		public int DispositivoId { get; set; }

        [Display(Name = "Usuario")]
        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        //[Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un usuario ")]
        public int UsuarioId { get; set; }


        [Display(Name = "Estado dispositivo")]
        public int EstadodispositivoId { get; set; }


        public virtual Usuario Usuario { get; set; }

        public virtual Dispositivo Dispositivo { get; set; }
				

        public virtual Tipo_Historico Tipo_Historico { get; set; }
    }
}