﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{

    [NotMapped]
    public class RecuperarContraseña
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "La contraseña debe contener mas de 6 carasteres")]
        public string Nuevacontraseña { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Nuevacontraseña", ErrorMessage = "Las contraseñas ingresadas no coinsiden, ingreselas nuevamente")]
        public string Confirmarcontraseña { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Recuperarcontraseña { get; set; }




      
    }
}