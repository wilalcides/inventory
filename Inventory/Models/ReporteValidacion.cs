﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class ReporteValidacion
    {

        public List<ReporteValidacion> ReporteVali { get; set; }



        public int ReporteValidacionId { get; set; }

        public string Sede { get; set; }
        public string Laboratorio { get; set; }
        public string Centro_de_costo { get; set; }
        public string Firma1 { get; set; }
        public string Firma2 { get; set; }
        public string Identificador { get; set; }
        public string Dispositivo { get; set; }
        public string Precio { get; set; }

        public string ObservacionFirmas { get; set; }
       
        public string Fecha_compra { get; set; }
        public string Observaciones { get; set; }
        public string Estado_Validacion { get; set; }
        public int UsuarioId { get; set; }

        //public virtual ICollection<ValidadorDetalle> ValidadorDetalle { get; set; }

        // public virtual ValidadorDetalle ValidadorDetalle { get; set; }
    }
}