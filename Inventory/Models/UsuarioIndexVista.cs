﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{

	[NotMapped]
	public class UsuarioIndexVista : Usuario
	{
	
		[Display(Name = "Rol de Administrador")]
		public bool EsAdministrador { get; set; }

        [Display(Name = "Estado")]
        public string Estado_vista { get; set; }



        


    }
}