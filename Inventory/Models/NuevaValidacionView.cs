﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class NuevaValidacionView
    {

        public List<NuevaValidacionView> ValiDetTmp { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Identificador { get; set; }

        public string Dispositivo { get; set; }

        [Display(Name = "Centro de costos")]
        public string Centrodecostos { get; set; }

        [Display(Name = "Fecha de compra")]
        public DateTime Fechacompra { get; set; }

        public string Costo { get; set; }

        [Display(Name = "Estado Dispositivo")]
        public string Estado { get; set; }

        public string Sede { get; set; }

        public int ValidadetalleTmpId { get; set; }
        
        public string Laboratorio { get; set; }

        [DataType(DataType.MultilineText)]
        
        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        [Display(Name = "Estado de Validacion")]
        public string EstadoInventario { get; set; }

        public string DispoValidado { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha validacion")]
        public DateTime Fecha { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Firmausuario1 { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Firmausuario2 { get; set; }





        
       public List<ValidadorDetalleTmp> Detalles { get; set; }





    }
}