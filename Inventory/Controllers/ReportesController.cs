﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using Inventory.Clases;
using Inventory.Models;

namespace Inventory.Controllers
{
   
    public class ReportesController : Controller
    {
        private InventoryContext db = new InventoryContext();

       
        // GET: Reportes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Reporte reporte = db.Reportes.Find(id);

            if (reporte == null)
            {
                return HttpNotFound();
            }

            return View(reporte);
        }


        // GET: Reportes/Create
        public ActionResult Create()
        {
            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            MetodosHelper.EliminaReporte(usutemp.Usuarioid);           

            //Iniciar con la fecha del sistema
            Reporte model = new Reporte() { Reportedisp = new List<Reporte>() };
			{
				model.FechaInicio = System.DateTime.Now;
				model.FechaFinal = System.DateTime.Now;
			};

            if(usutemp.Rol == "false")
            {
                model.SedeId = usutemp.SedeId;
                model.LaboratorioId = usutemp.LaboratorioId;
            }
			else
			{
				usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
			}		

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre");

            ViewBag.SedeId = new SelectList(
                CombosHelper.GetSedes(),
                "SedeId",
                "Nombre");

            ViewBag.EstadoId = new SelectList(
                            CombosHelper.GetEstados(),
                            "EstadoId",
                            "Nombre");			

            return View(model);
        }


        public JsonResult GetLaboratorios(int sedeid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var laboratorios = db.Laboratorios.Where(c => c.SedeId == sedeid);
            return Json(laboratorios);
        }


        // POST: Reportes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Reporte rep )
        {            
			var com = 0;

            Reporte reporvista = new Reporte() { Reportedisp = new List<Reporte>() };

            if (ModelState.IsValid)
            {
                var autentic = User.Identity.IsAuthenticated;
                var nametemp = User.Identity.Name;
                var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                            
                var dispousuarios = db.Dispositivos.Where(d => d.EstadoId == rep.EstadoId && d.Fecha >= rep.FechaInicio && d.Fecha <= rep.FechaFinal && d.LaboratorioId == rep.LaboratorioId && d.SedeId == rep.SedeId).ToList();
                               
                foreach (var req in dispousuarios)
                {
                    Reporte report = new Reporte()
                    {
                        FechaFinal = rep.FechaFinal,
                        FechaInicio = rep.FechaInicio,
                        EstadoId = rep.EstadoId,
                        Estatus = db.Estados.Where(e => e.EstadoId == req.EstadoId).Select(e => e.Nombre).FirstOrDefault(),
                        Identificador = req.Identificador,
                        Nombre = req.Nombre,
                        Marca = req.Marca,
                        Modelo = req.Modelo,
                        Precio = req.Precio.ToString(),
                        Serial = req.Serial,
                        Fecha = req.Fecha,
                        Sede = db.Sedes.Where(s => s.SedeId == req.SedeId).Select(s => s.Nombre).FirstOrDefault(),
                        Laboratorio = db.Laboratorios.Where(l => l.LaboratorioId == req.LaboratorioId).Select(l => l.Nombre).FirstOrDefault(),
                        SedeId = rep.SedeId,
                        LaboratorioId = rep.LaboratorioId,
                        UsuarioId = usutemp.Usuarioid
                    };
                                            
                    if (report != null)
					{
						com = com + 1;
					}

                    db.Reportes.Add(report);
                    db.SaveChanges();
                }

				if (com == 0)
				{
                    ViewBag.Message = "No hay registros con el estado seleccionado";
				}

                // carga los registros para mostrar en la vista
				foreach (var req in db.Reportes.Where(u => u.UsuarioId == usutemp.Usuarioid).ToList())
                {
                    Reporte reporttmp = new Reporte();

                    reporttmp.FechaFinal = rep.FechaFinal;
                    reporttmp.FechaInicio = rep.FechaInicio;
                    reporttmp.EstadoId = rep.EstadoId;
                    reporttmp.Estatus = req.Estatus;
                    reporttmp.Identificador = req.Identificador;
                    reporttmp.Nombre = req.Nombre;
                    reporttmp.Marca = req.Marca;
                    reporttmp.Modelo = req.Modelo;
                    reporttmp.Precio = req.Precio;
                    reporttmp.Serial = req.Serial;
                    reporttmp.Fecha = req.Fecha;
                    reporttmp.Sede = req.Sede;
                    reporttmp.Laboratorio = req.Laboratorio;

                    reporvista.Reportedisp.Add(reporttmp);
                }

                //Se realiza el registro del evento para temas de seguridad y trazabilidad      
                if (autentic)
                {
                    var eventtemp = new Eventos();
                    var esttmp = db.Estados.Where(e => e.EstadoId == rep.EstadoId).Select(n => n.Nombre).FirstOrDefault();

                    eventtemp.Fechcha = System.DateTime.Now;
                    eventtemp.UsuarioId = usutemp.Usuarioid;
                    eventtemp.Descripcion = "El usuario logueado genero un reporte de dispositivos de estado " + esttmp + ".";

                    db.Eventos.Add(eventtemp);
                    db.SaveChanges();
                }                
            }

			var id = rep.SedeId;

            ViewBag.EstadoId = new SelectList(
                 CombosHelper.GetEstados(),
                    "EstadoId",
                     "Nombre");
           
            ViewBag.LaboratorioId = new SelectList(
                    CombosHelper.GetLaboratoriosed(id),
                    "LaboratorioId",
                    "Nombre");

            ViewBag.SedeId = new SelectList(
                CombosHelper.GetSedes(),
                "SedeId",
                "Nombre");

            return View(reporvista);                        
        }


        //Metodo para generar y exportar el archivo en formato pdf
        public ActionResult PDF()
        {
            var reportdisp = this.GenerarReporteDisp();
            var stream = reportdisp.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            //Se realiza el registro del evento para temas de seguridad y trazabilidad
            var autentic = User.Identity.IsAuthenticated;
            var eventtemp = new Eventos();

            if (autentic)
            {
                var nametemp = User.Identity.Name;
                var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                eventtemp.Fechcha = System.DateTime.Now;
                eventtemp.UsuarioId = usutemp.Usuarioid;
                eventtemp.Descripcion = "El usuario logueado realizo la exportacion del reporte a formato pdf ";

                db.Eventos.Add(eventtemp);
                db.SaveChanges();
            }
			
			return File(stream, "application/pdf");
		}

       
        //Metodo que realiza la consulta de la informacion del reporte en la base de datos 
        private ReportClass GenerarReporteDisp()
        {    
            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            var Id = usutemp.Usuarioid;

            //como para el reporte no se conecta por entitiframework se debe hacer por ado
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var connections = new SqlConnection(connectionString);
            var dataTable = new DataTable();
            var sql = "select * from Reportes where UsuarioId = @Id ORDER BY Nombre";

            try
            {
                connections.Open();
                // en estas lineas se envia la instruccion sql con el parametro id para generar el reporte solo con lo relacionado al usuario logueado
                var conmand = new SqlCommand(sql, connections);

                conmand.Parameters.Add("@Id", SqlDbType.Int);
                conmand.Parameters["@Id"].Value = Id;

                var adapter = new SqlDataAdapter(conmand);

                adapter.Fill(dataTable);  
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }

            var report = new ReportClass();

            report.FileName = Server.MapPath("/Reports/ReporteDispo.rpt");
            report.Load();
            report.SetDataSource(dataTable);

			MetodosHelper.EliminaReporte(usutemp.Usuarioid);

			return report;
        }
                 

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
