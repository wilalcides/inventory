﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Models;


namespace Inventory.Controllers
{
    public class HistoricosController : Controller
    {
        private InventoryContext db = new InventoryContext();


        // GET: Historicos
        public ActionResult Index(int? id)
        {
          var Histor = db.Historicos.Where(d => d.DispositivoId == id).Include(t => t.Tipo_Historico).Include(d => d.Dispositivo).Include(u => u.Usuario);           

          return View(Histor.ToList());                
        }


        // GET: Historicos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Historico historico = db.Historicos.Find(id);

            if (historico == null)
            {
                return HttpNotFound();
            }

            return View(historico);
        }


        // GET: Historicos/Create
        public ActionResult Create(int? id , int? id2)
        {
            if (id2 == 1)
            {
                ViewBag.Message = "Dispositivo actualizado con exito, debe registrar el historico de los cambios";
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var autentic = User.Identity.IsAuthenticated;

            var hitor = new Historico();

            if (autentic)
            {
                var nametemp = User.Identity.Name;
                var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                Dispositivo distemp = db.Dispositivos.Find(id);

                hitor.Fecha = System.DateTime.Now;
                hitor.UsuarioId = usutemp.Usuarioid;
                hitor.DispositivoId = distemp.DispositivoId;
                hitor.EstadodispositivoId = distemp.EstadoId;              
            }
            
            ViewBag.TipoId = new SelectList(
                CombosHelper.GetTipo(),
                "TipoId",
                "Nombre");

            ViewBag.EstadodispositivoId = new SelectList(
               CombosHelper.GetEstados(),
               "EstadoId",
               "Nombre",hitor.EstadodispositivoId);

            return View(hitor);
        }


        // POST: Historicos/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Historico historico)
        {
            if (ModelState.IsValid)
            {
                var disptmp = db.Dispositivos.Where(d => d.DispositivoId == historico.DispositivoId).FirstOrDefault();

                disptmp.EstadoId = historico.EstadodispositivoId;

                db.Entry(disptmp).State = EntityState.Modified;

                db.Historicos.Add(historico);

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;

                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        var dispotemp = db.Dispositivos.Where(d => d.DispositivoId.Equals(historico.DispositivoId)).Select(n => n.Nombre).FirstOrDefault();
                        
                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado registro un historico del dispositivo " + dispotemp + ".";
                        eventtemp.IdRegModificado = historico.DispositivoId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                               
                return RedirectToAction("Index","Dispositivos", new { id = 1});
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&                    
                    ex.InnerException.InnerException.Message.Contains("FK_dbo.Historicoes_dbo.Tipo_Historico_TipoId"))
                    {
                        ViewBag.Message = "Debe elegir un tipo de mantenimiento";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                   
                }
            }

            ViewBag.EstadodispositivoId = new SelectList(
               CombosHelper.GetEstados(),
               "EstadoId",
               "Nombre", historico.EstadodispositivoId);

            ViewBag.TipoId = new SelectList(
               CombosHelper.GetTipo(),
               "TipoId",
               "Nombre");
                       
            return View(historico);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
