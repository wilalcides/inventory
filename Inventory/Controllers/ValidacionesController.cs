﻿using CrystalDecisions.CrystalReports.Engine;
using Inventory.Clases;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    public class ValidacionesController : Controller
    {
        private InventoryContext db = new InventoryContext();

        //metodo que exporta el reporte a un archivo en formato pdf y lo muestra en una pestaña del navegador
        public ActionResult PDF()
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            // segun el id del usuario consulta en la cabecera de la validacion si existen las firmas
            var validafirma = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).Select(f => f.Firmausuario1).FirstOrDefault();

            // si no existen las firmas se carga un mensaje para motrarlo en el reporte vacio
            if (validafirma == null)
            {
                ReporteValidacion reportfinaltmp = new ReporteValidacion();

                reportfinaltmp.ObservacionFirmas = "Debe agregar las firmas antes de exportar el reporte de validacion";
                reportfinaltmp.Fecha_compra = " ";
                reportfinaltmp.Precio = " ";


                db.ReporteValidaciones.Add(reportfinaltmp);
                db.SaveChanges();
            }

            // consulta la informacion necesaria para el reporte
            string ruta1 = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).Select(f => f.Firmausuario1).FirstOrDefault();
            string ruta2 = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).Select(f => f.Firmausuario2).FirstOrDefault();
            string path1 = string.Empty;
            string path2 = string.Empty;
            path1 = Server.MapPath(ruta1);
            path2 = Server.MapPath(ruta2);

            var reportdisp = this.GenerarReporteVali();

            reportdisp.SetParameterValue("Firma1", path1);
            reportdisp.SetParameterValue("Firma2", path2);

            // realiza la exportacion del eporte
            var stream = reportdisp.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            //Se realiza el registro del evento para temas de seguridad y trazabilidad
            var eventtemp = new Eventos();

            if (autentic)
            {

                eventtemp.Fechcha = System.DateTime.Now;
                eventtemp.UsuarioId = usutemp.Usuarioid;
                eventtemp.Descripcion = "El usuario logueado realizo la exportacion del reporte de validacion a formato pdf ";

                db.Eventos.Add(eventtemp);
                db.SaveChanges();
                
            }
            
            // se borra el contenido de las tablas del reporte
            if (validafirma != null)
            {

                MetodosHelper.EliminarValDetaTmp(usutemp.Usuarioid);
                MetodosHelper.EliminarRepVal(usutemp.Usuarioid);
               
                foreach (var dtmp in db.Dispositivos.Where(u => u.LaboratorioId == usutemp.LaboratorioId))
                {
                    dtmp.EstadoInventario = "No Inventariado";
                    dtmp.Observaciones = "";
                    db.Entry(dtmp).State = EntityState.Modified;
                    
                }
                
                db.SaveChanges();

                Validador validacion = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).FirstOrDefault();

                validacion.Firmausuario1 = null;
                validacion.Firmausuario2 = null;
                db.Entry(validacion).State = EntityState.Modified;
                db.SaveChanges();

            }

            // lleva a una nueva ventana en la que muestra el reporte
            return File(stream, "application/pdf");

        }

        
        // Get Firmar validacion muestra formulario para ingresar las imagenes de las firmas
        public ActionResult FirmarValidacion(int? id)
        {

            // se utiliza el parametro id para validar si se registraron las firmas en el metodo post previamente
            if (id != null)
            {

                ViewBag.Message = "Las firmas ya fueron guardadas con anterioridad, por favor hacer la exportacion del reporte";

            }

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            Validador validacion = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).FirstOrDefault();

            if (validacion == null)
            {

                return HttpNotFound();

            }

            validacion.Fecha = System.DateTime.Now;
            validacion.UsuarioId = usutemp.Usuarioid;
            validacion.SedeId = usutemp.SedeId;
            validacion.LaboratorioId = usutemp.LaboratorioId;
            validacion.CentrodecostoId = db.Centrodecostos.Where(c => c.SedeId == usutemp.SedeId).Select(i => i.CentrodecostoId).FirstOrDefault();
            validacion.Firmausuario1 = null;
            validacion.Firmausuario2 = null;

            return View(validacion);

        }

               
        //Post: Firmar validacion este metodo registra las firmas
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FirmarValidacion(Validador validacion)
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                       
            var verificar = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).Select(f => f.Firmausuario1).FirstOrDefault();

            // se valida si existen las firmas y muestra un mensaje de error si existen
            if (verificar == null)
            {
                
                var valitemp = db.ValidadorDetalles.Where(u => u.UsuarioId == usutemp.Usuarioid).ToList();

                //se valida que si existan equipos validados
                if (valitemp.Count != 0)
                {

                    //se valida que si se hallan ingresado las firmas
                    if ((validacion.Firmausuario1Imagen != null && validacion.Firmausuario2Imagen != null))
                    {

                        var tam1 = validacion.Firmausuario1Imagen.ContentLength;
                        var tam2 = validacion.Firmausuario2Imagen.ContentLength;

                        // se valida que el tamaño de la imagen no sea exagerado
                        if ((tam1 < 7500) && (tam2 < 7500))
                        {

                            // en esta parte se suben las imagenes al seridor y se guarda la ruta en la base de datos
                            var firma1 = string.Empty;
                            var firma2 = string.Empty;
                            var folder = "~/Content/Firmas";

                            firma1 = MetodosHelper.SubirfFirma(validacion.Firmausuario1Imagen, folder);
                            firma1 = string.Format("{0}/{1}", folder, firma1);
                            firma2 = MetodosHelper.SubirfFirma(validacion.Firmausuario2Imagen, folder);
                            firma2 = string.Format("{0}/{1}", folder, firma2);

                            validacion.Firmausuario1 = firma1;
                            validacion.Firmausuario2 = firma2;

                            db.Entry(validacion).State = EntityState.Modified;

                            // en esta parte se carga toda la informacion para el reporte en la tabla de la base de datos, esta tabla es utilizada por el drystal reports
                            ReporteValidacion reportfinal = new ReporteValidacion();

                            var cabezatmp = db.Validaciones.Find(validacion.ValidadorId);

                            foreach (var tmp in db.ValidadorDetalles.Where(u => u.UsuarioId == usutemp.Usuarioid).ToList())
                            {

                                reportfinal.Sede = db.Sedes.Where(s => s.SedeId == cabezatmp.SedeId).Select(n => n.Nombre).FirstOrDefault();
                                reportfinal.Laboratorio = db.Laboratorios.Where(s => s.LaboratorioId == cabezatmp.LaboratorioId).Select(n => n.Nombre).FirstOrDefault();
                                reportfinal.Centro_de_costo = db.Centrodecostos.Where(s => s.CentrodecostoId == cabezatmp.CentrodecostoId).Select(n => n.Nombre).FirstOrDefault();
                                reportfinal.Identificador = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.Identificador).FirstOrDefault();
                                reportfinal.Dispositivo = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.Nombre).FirstOrDefault();
                                reportfinal.Precio = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.Precio).FirstOrDefault();
                                reportfinal.Observaciones = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.Observaciones).FirstOrDefault();
                                reportfinal.Estado_Validacion = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.EstadoInventario).FirstOrDefault();
                                var tmpfecha = db.Dispositivos.Where(d => d.DispositivoId == tmp.DispositivoId).Select(i => i.Fecha).FirstOrDefault();

                                reportfinal.Fecha_compra = Convert.ToString(tmpfecha);
                                reportfinal.UsuarioId = usutemp.Usuarioid;

                                db.ReporteValidaciones.Add(reportfinal);
                                db.SaveChanges();

                            }


                            MetodosHelper.EliminarValDet(usutemp.Usuarioid);
                            ViewBag.Message = "Las firmas ya fueron registradas, por favor realice la exportacion del informe y luego elija un modulo para continuar";

                        }
                        else
                        {

                            ViewBag.Message = "Las imagenes de las firmas deben tener un tamaño  de 300 a 350 pixeles de ancho y de 80 a 100 pixeles de alto o tener un peso menor a 7 KB ";

                        }

                    }
                    else
                    {

                        ViewBag.Message = "Debe ingresar las dos firmas solicitadas, para finalizar la validacion ";

                    }

                }
                else
                {

                    ViewBag.Message = "Debe iniciar una nueva validacion, ingresando al modulo validador para iniciar el proceso ";

                }

            }
            else
            {

                ViewBag.Message = "Las firmas ya fueron registradas con anterioridad y no pueden ser reemplazadas , por favor realice la exportacion del informe y luego elija un modulo para continuar ";

            }

            return View(validacion);

        }

        
        // metodo para generar el reporte y cargarlo en el dataset 
        private ReportClass GenerarReporteVali()
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            var Id = usutemp.Usuarioid;

            //como para el reporte no se conecta por entitiframework se debe hacer por ado
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var connections = new SqlConnection(connectionString);
            var dataTable = new DataTable();
            var sql = "select * from ReporteValidacions where UsuarioId = @Id";

            try
            {

                connections.Open();
                // en estas lineas se envia la instruccion sql con el parametro id para generar el reporte solo con lo relacionado al usuario logueado
                var conmand = new SqlCommand(sql, connections);

                conmand.Parameters.Add("@Id", SqlDbType.Int);
                conmand.Parameters["@Id"].Value = Id;

                var adapter = new SqlDataAdapter(conmand);
                adapter.Fill(dataTable);

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(string.Empty, ex.Message);

            }

            var report = new ReportClass();
                       
            report.FileName = Server.MapPath("/Reports/ReporteValidacion.rpt");

            report.Load();
            report.SetDataSource(dataTable);

            return report;

        }
        

        //Get Mostrar dispositivos con su estado de inventario validado y no validado, permite agregar alguna observacion si es necesario
        public ActionResult MostrarValidados()
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            // se consulta si ya existen firmas agregadas
            var Analifirma = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).FirstOrDefault();

			if (Analifirma != null)
			{


				// se valida y si existen firmas se redirige al usuario a la opcion exporta reporte
				if (Analifirma.Firmausuario1 != null && Analifirma.Firmausuario1 != " ")
				{

					return RedirectToAction("FirmarValidacion", "Validaciones", new { id = 1 });

				}

			}

			//Se valida si existen dispositivos del laboratorio del usuario logueado
			var contadisp = db.Dispositivos.Where(u => u.LaboratorioId == usutemp.LaboratorioId && u.SedeId == usutemp.SedeId).FirstOrDefault();

			if (contadisp == null)
			{
				//ModelState.AddModelError(string.Empty, "No hay dispositivo asociados su laboratorio");
				
				return RedirectToAction("Create", "Validaciones", new {id = 2 });
			}

            // se limpian los registros segun el id del usuario loguead para iniciar un nuevo reporte
            MetodosHelper.EliminarRepVal(usutemp.Usuarioid);
            MetodosHelper.EliminarValDet(usutemp.Usuarioid);
            
            // se consulta y se valida si existe una cabecera de validador  si no existe la crea y si existe la actualiza
            var tempconsul = db.Validaciones.Where(U => U.UsuarioId == usutemp.Usuarioid).FirstOrDefault();

            if (tempconsul == null)
            {

                Validador valcabeza = new Validador();
                valcabeza.UsuarioId = usutemp.Usuarioid;
                valcabeza.Fecha = System.DateTime.Now;
                valcabeza.SedeId = usutemp.SedeId;
                valcabeza.LaboratorioId = usutemp.LaboratorioId;
                valcabeza.CentrodecostoId = db.Centrodecostos.Where(c => c.SedeId == usutemp.SedeId).Select(i => i.CentrodecostoId).FirstOrDefault();

                db.Validaciones.Add(valcabeza);

                db.SaveChanges();

            }
            else
            {

                tempconsul.UsuarioId = usutemp.Usuarioid;
                tempconsul.Fecha = System.DateTime.Now;
                tempconsul.SedeId = usutemp.SedeId;
                tempconsul.LaboratorioId = usutemp.LaboratorioId;
                tempconsul.CentrodecostoId = db.Centrodecostos.Where(c => c.SedeId == usutemp.SedeId).Select(i => i.CentrodecostoId).FirstOrDefault();
                tempconsul.Firmausuario1 = null;
                tempconsul.Firmausuario2 = null;
                
                db.Entry(tempconsul).State = EntityState.Modified;

                db.SaveChanges();

            }
            
            // se registran en la tabla validador detalle todos los dispositivos que corresponden al laboratorio del usuario legueado
            var cont = db.Dispositivos.Where(l => l.LaboratorioId.Equals(usutemp.LaboratorioId));

            var cabeza = db.Validaciones.Where(u => u.UsuarioId == usutemp.Usuarioid).FirstOrDefault();

            foreach (var disp in cont)
            {

                ValidadorDetalle valiview = new ValidadorDetalle();

                valiview.DispositivoId = disp.DispositivoId;
                valiview.UsuarioId = usutemp.Usuarioid;
                valiview.ValidadorId = cabeza.ValidadorId;
                valiview.UsuarioId = usutemp.Usuarioid;
                
                db.ValidadorDetalles.Add(valiview);

            }

            db.SaveChanges();
            
            return RedirectToAction("Index", "validaciones");

        }


        // GET: Muestra el listado de los dispositivos 
        public ActionResult Index()
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            var validaciones = db.ValidadorDetalles.Where(u => u.UsuarioId == usutemp.Usuarioid).ToList();
            
            return View(validaciones);

        }


        // este metodo carga un objeto con la informacion que se mostrara en la vista validador, dispositivos ya validados
        public object Vistavalidados()
        {

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            NuevaValidacionView model = new NuevaValidacionView() { ValiDetTmp = new List<NuevaValidacionView>() };

            foreach (var dispotmp in db.ValidadorDetalleTmps.Where(u => u.UsuarioId == usutemp.Usuarioid).ToList())
            {
                
                var objdis = db.Dispositivos.Where(d => d.DispositivoId == dispotmp.DispositivoId).FirstOrDefault();

                NuevaValidacionView valiview = new NuevaValidacionView();

                valiview.Identificador = objdis.Identificador;
                valiview.Dispositivo = objdis.Nombre;
                valiview.Fechacompra = objdis.Fecha;
                valiview.Costo = objdis.Precio;
                valiview.Estado = db.Estados.Where(e => e.EstadoId == objdis.EstadoId).Select(e => e.Nombre).FirstOrDefault();
                valiview.EstadoInventario = objdis.EstadoInventario;
                valiview.Laboratorio = db.Laboratorios.Where(l => l.LaboratorioId == objdis.LaboratorioId).Select(l => l.Nombre).FirstOrDefault();
                valiview.Centrodecostos = db.Laboratorios.Where(l => l.LaboratorioId == objdis.LaboratorioId).Select(l => l.Centrodecosto.Nombre).FirstOrDefault();
                valiview.Sede = db.Laboratorios.Where(l => l.LaboratorioId == objdis.LaboratorioId).Select(l => l.Sede.Nombre).FirstOrDefault();
                valiview.ValidadetalleTmpId = db.ValidadorDetalleTmps.Where(d => d.DispositivoId == objdis.DispositivoId).Select(i => i.ValidadorDetalleTmpId).FirstOrDefault();
                valiview.DispoValidado = " ";

                model.ValiDetTmp.Add(valiview);

            }

            return model;

        }


        // GET: Create: Metodo que muestra enla vista formulario para hacer la consulta de validacion y tambien 
        // muestra el listado de los dispositivos que se van consultando o registrando
        public ActionResult Create(int? id)
        {

            // se valida si el parametro id tiene un valor y eso significa que se realizo el registro previo
            if (id == 1)
            {

                ModelState.AddModelError(string.Empty, "Dispositivo agregado a la lista");

            }

			if (id == 2)
			{
				ViewBag.Message = "No hay dispositivos asociados a su laboratorio";
			}

            var objvista = this.Vistavalidados();

            return View(objvista);

        }


        // POST: Create:  este metodo realiza la acion de consultar actualizar y registrar un dispositivo
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NuevaValidacionView dsp)
        {

            NuevaValidacionView validacionview = new NuevaValidacionView() { ValiDetTmp = new List<NuevaValidacionView>() };

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            // Valida que el campo no este vacio o nulo
            if (dsp.DispoValidado != null)
            {

                Dispositivo dispotmp = new Dispositivo();

                dispotmp = db.Dispositivos.Where(d => d.Identificador == dsp.DispoValidado).FirstOrDefault();

                //validacion si el numero ingresado no existe en la base de datos
                if (dispotmp != null)
                {

                    // valida si el dispositivo si pertenece al laboratorio al que esta asignado el usuario
                    if (dispotmp.LaboratorioId != usutemp.LaboratorioId)
                    {

                        ViewBag.Message = "Este dispositivo no esta asignado a su laboratorio";

                        var objvista = this.Vistavalidados();

                        return View(objvista);

                    }
                    
                    // si encuentra el dispositivo actualiza el estado de inventario y lo agrega en la tabla validador detalletemporal 
                    // para que el usuario valla a perder los dispositivos validados
                    // si el dispositivo ya tiene el estado de inventariado muestra un mensaje de error
                    if (dispotmp.EstadoInventario != "Inventariado")
                    {

                        dispotmp.EstadoInventario = "Inventariado";

                        db.Entry(dispotmp).State = EntityState.Modified;

                        db.SaveChanges();

                        ValidadorDetalleTmp validTmp = new ValidadorDetalleTmp
                        {

                            DispositivoId = dispotmp.DispositivoId,

                            UsuarioId = usutemp.Usuarioid

                        };

                        db.ValidadorDetalleTmps.Add(validTmp);

                        db.SaveChanges();

                    }
                    else
                    {

                        ViewBag.Message = "El dispositivo ya fue validado";

                        var objvista = this.Vistavalidados();

                        return View(objvista);

                    }

                }
                else
                {

                    ViewBag.Message = "No existe un dispositivo con ese identificador";

                    var objvista = this.Vistavalidados();

                    return View(objvista);

                }

            }
            else
            {

                ViewBag.Message = "Debe ingresar un identificador de dispositivo";

                var objvista = this.Vistavalidados();
                
                return View(objvista);

            }
            
            return RedirectToAction("Create", "Validaciones", new { id = 1 });

        }


        // GET: Edit muestra formulario para registrar una observacion sobre el dispositivo con estado no inventariado
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }

            var valdetalletmp = db.ValidadorDetalles.Find(id);

            ModelConfirmarView validador = new ModelConfirmarView();

            validador.ValidadorDetalleId = valdetalletmp.ValidadorDetalleId;
            validador.UsuarioId = valdetalletmp.UsuarioId;
            validador.ValidadorId = valdetalletmp.ValidadorId;
            validador.DispositivoId = valdetalletmp.DispositivoId;

            if (validador == null)
            {

                return HttpNotFound();

            }
            
            return View(validador);

        }


        // POST: Edit: este metodo permite hacer el registro de las observaciones de los dispositivos que esten estado no inventariado
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ModelConfirmarView validadorDtmp)
        {

            // consulta el dispositivo
            var dtmp = db.Dispositivos.Where(d => d.DispositivoId == validadorDtmp.DispositivoId).Select(e => e.EstadoInventario).FirstOrDefault();

            // valida el valor del campo si es diferente de no iventariado no le permitira registrar observaciones
            if (dtmp == "No Inventariado")
            {

                // valida si el campo observaciones esta vacio o nulo
                if (validadorDtmp.Observaciones != null)
                {

                    if (ModelState.IsValid)
                    {

                        var Diptmp = db.Dispositivos.Where(d => d.DispositivoId == validadorDtmp.DispositivoId).FirstOrDefault();

                        Diptmp.Observaciones = validadorDtmp.Observaciones;
                        db.Entry(Diptmp).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");

                    }

                    ViewBag.UsuarioId = new SelectList(db.Usuarios, "Usuarioid", "Logueo", validadorDtmp.UsuarioId);
                    return View(validadorDtmp);

                }
                else
                {

                    ViewBag.Message = "Debe registrar por lo menos una obserbacion";

                }

            }
            else
            {

                ViewBag.Message = "Las observaciones solo son para dispositivos con estado No inventariado ";

            }

            return View(validadorDtmp);

        }
        

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {

                db.Dispose();

            }

            base.Dispose(disposing);

        }

    }

}
