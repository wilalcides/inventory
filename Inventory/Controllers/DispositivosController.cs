﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Inventory.Models;


namespace Inventory.Controllers
{
        
    public class DispositivosController : Controller
    {
        private InventoryContext db = new InventoryContext();

        // Metodo para el manejo del comobobox
        public JsonResult GetLaboratorios(int sedeid)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var laboratorios = db.Laboratorios.Where(c => c.SedeId == sedeid && c.Estado == "Activo");

            return Json(laboratorios);
        }


        // GET: Dispositivos
        public ActionResult Index(int? id)
        {
            if (id == 1)
            {
                ViewBag.Message = "Actualizacion de datos, estado y registro de historico con exito";
            }

            var autentic = User.Identity.IsAuthenticated;
            var nametemp = User.Identity.Name;
            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

            if (User.IsInRole("Administrador"))
            {
                var Dispositivosadmin = db.Dispositivos.Include(d => d.Laboratorio);

                return View(Dispositivosadmin.ToList());
            }
            else
            {
              var Dispositivos = db.Dispositivos.Where(d => d.LaboratorioId == usutemp.LaboratorioId).ToList();

              return View(Dispositivos);
            }
        }


        // GET: Dispositivos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Dispositivo dispositivo = db.Dispositivos.Find(id);

            if (dispositivo == null)
            {
                return HttpNotFound();
            }

            return View(dispositivo);
        }


        [Authorize (Roles = "Administrador")]
        // GET: Dispositivos/Create
        public ActionResult Create(int? id)
        {
            //estas lines e el get se utilizan para dejar el dispositivo con estado activo en su creacion
            Dispositivo dispositivo = new Dispositivo
            {
                EstadoId = 1,
                Fecha = System.DateTime.Now,
                EstadoInventario = "No validado"
            };

            if (id != null)
            {
                ViewBag.Message = "Dispositivo registrado con exito";
            }


            ViewBag.SedeId = new SelectList(
                CombosHelper.GetSedes(),
                "SedeId",
                "Nombre");

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre");
            
            ViewBag.EstadoId = new SelectList(
                CombosHelper.GetEstados(),
                "EstadoId",
                "Nombre");
            
			return View(dispositivo);
		}


        // POST: Dispositivos/Create        
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Dispositivo dispositivo)
        {
            if (ModelState.IsValid)
            {      
                db.Dispositivos.Add(dispositivo);
                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado registro un dispositivo";
                        eventtemp.IdRegModificado = dispositivo.DispositivoId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                   
                    ViewBag.Message = "Dispositivo registrado con exito";

                    ViewBag.LaboratorioId = new SelectList(
                       CombosHelper.GetLaboratorios(),
                       "LaboratorioId",
                       "Nombre", dispositivo.LaboratorioId);

                    ViewBag.EstadoId = new SelectList(
                        CombosHelper.GetEstados(),
                        "EstadoId",
                        "Nombre", dispositivo.EstadoId);

                    ViewBag.SedeId = new SelectList(
                         CombosHelper.GetSedes(),
                         "SedeId",
                         "Nombre", dispositivo.SedeId);

                    return RedirectToAction("Create", "Dispositivos", new { id = 1 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "El dispositivo con este numero de serial o identificador ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                   
                }                
            }

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre", dispositivo.LaboratorioId);            

            ViewBag.EstadoId = new SelectList(
                CombosHelper.GetEstados(),
                "EstadoId",
                "Nombre", dispositivo.EstadoId);

            ViewBag.SedeId = new SelectList(
                 CombosHelper.GetSedes(),
                 "SedeId",
                 "Nombre", dispositivo.SedeId);

            return View(dispositivo);
        }


        [Authorize(Roles = "Administrador")]
        // GET: Dispositivos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Dispositivo dispositivo = db.Dispositivos.Find(id);

            if (dispositivo == null)
            {
                return HttpNotFound();
            }

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratoriosed(dispositivo.SedeId),
                "LaboratorioId",
                "Nombre", dispositivo.LaboratorioId);

            ViewBag.SedeId = new SelectList(
               CombosHelper.GetSedes(),
               "SedeId",
               "Nombre", dispositivo.SedeId);

            ViewBag.EstadoId = new SelectList(
                CombosHelper.GetEstados(),
                "EstadoId",
                "Nombre", dispositivo.EstadoId);

            ViewBag.distemp = db.Dispositivos.Find(id);            
			
			return View(dispositivo);
        }


        // POST: Dispositivos/Edit/5      
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Dispositivo dispositivo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dispositivo).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        var dispotemp = db.Estados.Where(e => e.EstadoId.Equals(dispositivo.EstadoId)).Select(n => n.Nombre).FirstOrDefault();

                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado modifico los datos un dispositivo y quedo en estado " + dispotemp + ".";
                        eventtemp.IdRegModificado = dispositivo.DispositivoId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                    
                    var id = dispositivo.DispositivoId;
                    var id2 = 1;
                    return RedirectToAction("Create","Historicos", new { id, id2 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "El dispositivo con este numero de serial ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                   
                }
            }            

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre", dispositivo.LaboratorioId);

            ViewBag.SedeId = new SelectList(
              CombosHelper.GetSedes(),
              "SedeId",
              "Nombre", dispositivo.SedeId);

            ViewBag.EstadoId = new SelectList(
                CombosHelper.GetEstados(),
                "EstadoId",
                "Nombre", dispositivo.EstadoId);

            return View(dispositivo);
        }

  //      // GET: Dispositivos/Delete/5
  //      public ActionResult Delete(int? id)
  //      {
  //          if (id == null)
  //          {
  //              return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
  //          }
  //          Dispositivo dispositivo = db.Dispositivos.Find(id);
  //          if (dispositivo == null)
  //          {
  //              return HttpNotFound();
  //          }
  //          return View(dispositivo);
  //      }

  //      // POST: Dispositivos/Delete/5
  //      [HttpPost, ActionName("Delete")]
  //      [ValidateAntiForgeryToken]
  //      public ActionResult DeleteConfirmed(int id)
  //      {
  //          Dispositivo dispositivo = db.Dispositivos.Find(id);
  //          db.Dispositivos.Remove(dispositivo);
          

		//	try
		//	{
		//		db.SaveChanges();
		//		return RedirectToAction("Index");
		//	}
		//	catch (Exception ex)
		//	{
		//		if (ex.InnerException != null &&
		//			ex.InnerException.InnerException != null &&
		//			ex.InnerException.InnerException.Message.Contains("REFERENCE"))

		//		{
		//			ModelState.AddModelError(string.Empty, "el registro no puede ser eliminado por que tiene registros relacionados");
		//		}
		//		else
		//		{
		//			ModelState.AddModelError(string.Empty, ex.Message);
		//		}
		//	}
		//	return View(dispositivo);
		//}


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
