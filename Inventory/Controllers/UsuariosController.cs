﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using Inventory.Clases;
using Inventory.Models;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Inventory.Controllers
{

    public class UsuariosController : Controller
    {
        private InventoryContext db = new InventoryContext();


        [Authorize(Roles = "Administrador")]
        public JsonResult GetLaboratorios(int sedeid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var laboratorios = db.Laboratorios.Where(c => c.SedeId == sedeid && c.Estado == "Activo");

            return Json(laboratorios);
        }


        // GET: Usuarios
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(int? id)
        {
            if (id == 1)
            {
                ViewBag.Message = "Usuario actualizado con exito";
            }

            if (id ==2)
            {
                ViewBag.Message = "Estado modificado con exito";
            }

            if (id == 3)
            {
                ViewBag.Message = "El email se actualizo con exito, se envio un enlace para la activacion de la cuenta";
            }
            
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));           
            var usuarios = db.Usuarios.OrderBy(d => d.Estado).ThenBy(d => d.Nombre).ToList();
            var usuarioVista = new List<UsuarioIndexVista>();

            foreach (var usuario in usuarios)
            {
                var userASP = userManager.FindByEmail(usuario.Logueo);

                usuarioVista.Add(new UsuarioIndexVista
                {
                    Nombre = usuario.Nombre,
                    Apellidos = usuario.Apellidos,
                    Documento = usuario.Documento,
                    Cargo = usuario.Cargo,
                    Direccion = usuario.Direccion,
                    Estado = usuario.Estado,
                    Laboratorio = usuario.Laboratorio,
                    Telefono = usuario.Telefono,
                    LaboratorioId = usuario.LaboratorioId,
                    Usuarioid = usuario.Usuarioid,
                    Logueo = usuario.Logueo,
                    EsAdministrador = userASP != null && userManager.IsInRole(userASP.Id, "Administrador"),
                });
            }

            return View(usuarioVista);           
        }


        // GET: Usuarios/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Usuario usuario = db.Usuarios.Find(id);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            return View(usuario);
        }


        // Get para cambiar estado
        [Authorize(Roles = "Administrador")]
        public ActionResult Activar_Desactivar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Usuario usuario = db.Usuarios.Find(id);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            if (usuario.Estado == "Activo")
            {
                usuario.Estado = "true";
            }
            else
            {
                usuario.Estado = "false";
            }

            return View(usuario);
        }


        //Post Cambiar estado
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activar_Desactivar(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                if (usuario.Estado == "true")
                {
                    usuario.Estado = "Activo";
                }
                else
                {
                    usuario.Estado = "Inactivo";
                }

                db.Entry(usuario).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado modifico el estado del usuario";
                        eventtemp.IdRegModificado = usuario.Usuarioid;

                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                    
                    return RedirectToAction("Index", new { id = 2 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ModelState.AddModelError(string.Empty, "El registro no puede ser modificado");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                  
                }
            }

            return View(usuario);
        }


        // GET: Usuarios/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(int? id)
        {
            Usuario usuario = new Usuario
            {
                Estado = "Registro",
                Logueo = "",
                Contraseña = "",
                Confircontraseña = ""
            };

            if (id != null)
            {
                ViewBag.Message = "El registro fue exitoso, se envio un email " +
                             " para la activacion de la cuenta:" ;
            }

            ViewBag.LaboratorioId = new SelectList(
             CombosHelper.GetLaboratorios(),
             "LaboratorioId",
             "Nombre");

            ViewBag.SedeId = new SelectList(
             CombosHelper.GetSedes(),
             "SedeId",
             "Nombre");

            return View(usuario);
        }


        // POST: Usuarios/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Usuario usuario)
        {
            if (usuario.Contraseña != null)
            {                
                var val = db.Usuarios.Where(e => e.Logueo == usuario.Logueo).FirstOrDefault();

                if (val != null)
                {
                    ViewBag.Message = "Ya existe un usuario con ese email";

                    ViewBag.LaboratorioId = new SelectList(
                       CombosHelper.GetLaboratorios(),
                       "LaboratorioId",
                       "Nombre", usuario.LaboratorioId);

                    ViewBag.SedeId = new SelectList(
                       CombosHelper.GetSedes(),
                       "SedeId",
                       "Nombre", usuario.SedeId);

                    return View(usuario);
                }

                if (ModelState.IsValid)
                {                    
                    //se crea el codigo de activacion
                    usuario.CodigoActivacion = Guid.NewGuid();
                    usuario.VerificacionEmail = false;                    
                    this.CreateASPUser(usuario);                    
                    usuario.Valediasp = usuario.Logueo;
                    db.Usuarios.Add(usuario);                  
                    
                    try
                    {
                        db.SaveChanges();

                        var userContext = new ApplicationDbContext();
                        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));                        
                        var userASP = userManager.FindByName(usuario.Logueo);

                        //se envia el email al usuario
                        var mensaje = "1";

                        SendVerificationLinkEmail(mensaje, usuario.Contraseña,userASP.Email, usuario.CodigoActivacion.ToString(), "VerificarCuenta");

                        if (usuario.Rol == "true")
                        {
                            userManager.AddToRole(userASP.Id, "Administrador");
                        }
                        else
                        {
                            userManager.AddToRole(userASP.Id, "Usuario");
                        }

                        usuario.Contraseña = null;
                        usuario.Confircontraseña = null;

                        //Se realiza el registro del evento para temas de seguridad y trazabilidad
                        var autentic = User.Identity.IsAuthenticated;
                        var eventtemp = new Eventos();

                        if (autentic)
                        {
                            var nametemp = User.Identity.Name;
                            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                            eventtemp.Fechcha = System.DateTime.Now;
                            eventtemp.UsuarioId = usutemp.Usuarioid;
                            eventtemp.Descripcion = "El usuario logueado registro un usuraio";
                            eventtemp.IdRegModificado = usuario.Usuarioid;

                            db.Eventos.Add(eventtemp);
                            db.SaveChanges();
                        }      

                        ViewBag.LaboratorioId = new SelectList(
                           CombosHelper.GetLaboratorios(),
                           "LaboratorioId",
                           "Nombre", usuario.LaboratorioId);

                        ViewBag.SedeId = new SelectList(
                           CombosHelper.GetSedes(),
                           "SedeId",
                           "Nombre", usuario.SedeId);

                        return RedirectToAction("Create", "Usuarios", new { id = 1 });
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null &&
                        ex.InnerException.InnerException != null &&
                        ex.InnerException.InnerException.Message.Contains("_Index"))
                        {
                            ViewBag.Message = "Ya existe un usuario con ese documento";
                        }
                        else
                        {
                            ViewBag.Message = "Debe configurar la Cuenta de correo electronico con la que se envian las notificaciones" +
                                " activando el acceso de las aplicaciones poco seguras, el usuario fue creado, debe activarlo manualmente ";
						}
					}
                }
            }
            else
            {
                ViewBag.Message = "Debe ingresar una contraseña y confirmarla";
            }

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre", usuario.LaboratorioId);

            ViewBag.SedeId = new SelectList(
               CombosHelper.GetSedes(),
               "SedeId",
               "Nombre", usuario.SedeId);

            return View(usuario);
        }
                     

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            
            return RedirectToAction("Login", "Account");
        }
                          

        //se verifica la confirmacion de la activacion de la cuenta
        [Authorize(Roles = "Usuario")]
        [HttpGet]
        public ActionResult VerificarCuenta(string id)
        {
            var autentic = User.Identity.IsAuthenticated;
            var nausutmp = User.Identity.Name;

            if (autentic == true)
            {
                this.LogOut();
            }
            
            db.Configuration.ValidateOnSaveEnabled = false;

            var v = db.Usuarios.Where(a => a.CodigoActivacion == new Guid(id)).FirstOrDefault();

            if (v != null)
            {
                v.VerificacionEmail = true;
                v.Estado = "Activo";
                db.SaveChanges();
            }
            else
            {
                ViewBag.Message = "SE genero un error en la activacion comuniquese con el administrador del sistema";
                return RedirectToAction("Login", "Account");
            }

            return RedirectToAction("Index", "Home");           
        }


        // METODO PARA EL ENVIO DEL CORREO DE CONFIRMACION AL USUARIO
        [Authorize(Roles = "Administrador")]
        public void SendVerificationLinkEmail(string mensaje, string contraseña, string Email, string CodigoActivacion, string email = "VerificarCuenta")
        {
            var verifyUrl = "/Usuarios/" + email + "/" + CodigoActivacion;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            var fromEmail = new MailAddress("TEMPORAL@tempo.com", "TEMPORAL");

            string subject = "";
            string body = "";

            if (email == "VerificarCuenta")
            {
                if (mensaje == "1")
                {
                    subject = "El registro de su cuenta fue exitoso bienvenido";

                    body = "<br/><br/>Nos complace informarle que su cuenta se creó correctamente." +
                       "Haga clic en el siguiente enlace para verificar su cuenta y posteriormente ," +
                       "con el correo que suministro para el registro y con la contraseña:  " + contraseña +
                       " <br/><br/><a href=" + link + ">Para notificar la cuenta de clic aqui</a> ";
                }

                if (mensaje == "2")
                {
                    subject = "La actualizacion de su cuenta fue exitosa bienvenid@";

                    body = "<br/><br/>La actualizacion de la cuenta fue exitoso." +
                       "Antes de dar clic en el enlace  enviado debe reiniciar el navegador de internet" +
                       "Luego de clic en el siguiente enlace para verificar su cuenta y posteriormente inicie sesion ," +
                       "con el correo que suministro para el registro y con la contraseña:  " + contraseña +
                       " <br/><br/><a href=" + link + ">Para notificar la cuenta de clic aqui</a> ";
                }
                
                fromEmail = new MailAddress("itminventory@gmail.com", "CONFIRMACION CREACION DE CUENTA");
            }
            else if (email == "RecuperarContraseña")
            {
                if (mensaje == null)
                {
                    subject = "El restablecimiento de la contraseña esta casi listo";
                    body = "<br/>br/>Para finalizar el proceso de restablecimiento de la contraseña, por favor de clic en el siguiente enlace:" +
                        "<br/>br/>Tenga presente que la contraseña debe contener la inicial en mayuscula, combinacion de letras y numeros y caracter especial" +
                        "<br/><br/><a href=" + link + ">Para restablecer la contraseña de clic aqui</a>";
                }

                fromEmail = new MailAddress("itminventory@gmail.com", "CONFIRMACION RESTABLECIMIENTO DE CONTRASEÑA");
            }
      
            //se configura una cuenta creada para administrar los envios de las confirmacionees de creacion de las cuentas 
            var toEmail = new MailAddress(Email);
            var fromEmailPassword = "Inventory2020.";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }


        [HttpGet]
        public ActionResult Restablecercontraseña()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Restablecercontraseña(Restablecercontraseña correo)
        {
            var corretmp = db.Usuarios.Where(c => c.Logueo == correo.RestablecerContraseña).FirstOrDefault();

            if (corretmp != null)
            {
               if (corretmp.Estado != "Inactivo")
               {
                  string restablecerClave = Guid.NewGuid().ToString();
                  string mensage = null;
                  SendVerificationLinkEmail(mensage, corretmp.Contraseña, corretmp.Logueo, restablecerClave, "RecuperarContraseña");
                  corretmp.CodigoRecuperarContraseña = restablecerClave;

                  db.Configuration.ValidateOnSaveEnabled = false;

                  db.SaveChanges();

                  ViewBag.Message = "Se envio su solicitud de restablecimiento, a su buzon de correo llegara un link para continuar con los pasos del restablecimiento de la clave";                    
               }
               else
               {
                  ViewBag.Message = "El usuario esta inactivo, contacte al administrador del sistema";
               }
            }
            else
            {
               ViewBag.Message = "No existe una cuenta creada con ese correo electronico, por favor ingrese un correo de una cuenta creada en nuestro sistema";                   
            }      
           
            return View();
        }


        //Get para recuperar contraseña
        public ActionResult RecuperarContraseña(string Id)
        {
            var usatmp = db.Usuarios.Where(c => c.CodigoRecuperarContraseña == Id).FirstOrDefault();
            
            if (usatmp != null)
            {
                RecuperarContraseña model = new RecuperarContraseña();
                model.Recuperarcontraseña = Id;
                return View(model);

            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecuperarContraseña(RecuperarContraseña model)
        {
            if (ModelState.IsValid)
            {
                var userContext = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));

                var user = db.Usuarios.Where(c => c.CodigoRecuperarContraseña == model.Recuperarcontraseña).FirstOrDefault();

                if (user != null)
                {
                    user.CodigoRecuperarContraseña = " ";
                    db.Entry(user).State = EntityState.Modified;
                    db.Configuration.ValidateOnSaveEnabled = false;

                    db.SaveChanges();

                    var userASP = userManager.FindByName(user.Logueo);

                    user.Contraseña = model.Nuevacontraseña;
                    userManager.Delete(userASP);

                    this.CreateASPUser(user);
                                       
                    var userASProl = userManager.FindByName(user.Logueo);

                    if (user.Rol == "true")
                    {
                        userManager.AddToRole(userASProl.Id, "Administrador");
                    }
                    else
                    {
                        userManager.AddToRole(userASProl.Id, "Usuario");
                    }
                    
                    return RedirectToAction("Login", "Account", new { id = 1 });
                }
            }
            else
            {
                ViewBag.Message = " Datos Invalidos intentelo de nuevo";
            }

            return View(model);
        }


        // SE CREA EL USUARIO EN LAS TABLAS DEL ASPNET
        [Authorize(Roles = "Administrador")]
        private void CreateASPUser(Usuario usuario)
        {
            // User management
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            // Create User role
            string roleName = "Usuario";

            // Check to see if Role Exists, if not create it
            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }

            // Create the ASP NET User
            var userASP = new ApplicationUser
            {
                UserName = usuario.Logueo,
                Email = usuario.Logueo,
                PhoneNumber = usuario.Telefono,
                EmailConfirmed = usuario.VerificacionEmail,
                SecurityStamp = usuario.CodigoActivacion.ToString(),
            };

            //se utiliza para enviar los parametros para la creacion del usuario el primero es el usuario y el segundo la contraseña
            userManager.Create(userASP, usuario.Contraseña);

            // Add user to role
            userASP = userManager.FindByName(usuario.Logueo);
            userManager.AddToRole(userASP.Id, "Usuario");
        }


        // GET: Usuarios/Edit/
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Usuario usuario = db.Usuarios.Find(id);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            if (usuario.Estado == "Activo")
            {
                usuario.Estado = "true";
            }
            else
            {
                usuario.Estado = "False";
            }

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratoriosed(usuario.SedeId),
                "LaboratorioId",
                "Nombre", usuario.LaboratorioId);

            ViewBag.SedeId = new SelectList(
               CombosHelper.GetSedes(),
               "SedeId",
               "Nombre", usuario.SedeId);

            return View(usuario);
        }

    
        // POST: Usuarios/Edit
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario usuario)
        {
            var valeditcorreomensaje = "";

            if (ModelState.IsValid)
            {
                if (usuario.Estado == "true")
                {
                    usuario.Estado = "Activo";
                }
                else
                {
                    usuario.Estado = "Inactivo";
                }

                db.Entry(usuario).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    Usuario valiasp = new Usuario();

                    valiasp = db.Usuarios.Where(u => u.Usuarioid == usuario.Usuarioid).FirstOrDefault();

                    var userContext = new ApplicationDbContext();
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));

                    if (usuario.Valediasp == usuario.Logueo)
                    {
                        var userASP = userManager.FindByName(usuario.Logueo);

                        if (userASP != null)
                        {
                            if (usuario.Rol == "false")
                            {
                                userManager.RemoveFromRole(userASP.Id, "Administrador");
                                userManager.AddToRole(userASP.Id, "Usuario");
                            }
                            else
                            {
                                userManager.RemoveFromRole(userASP.Id, "Usuario");
                                userManager.AddToRole(userASP.Id, "Administrador");
                            }
                        }
                    }
                    else
                    {
                        valeditcorreomensaje = "verdadero";
                        var userASP = userManager.FindByName(usuario.Valediasp);
                        usuario.Contraseña = usuario.Logueo;
                        usuario.Confircontraseña = usuario.Logueo;
                        userManager.Delete(userASP);

                        this.CreateASPUser(usuario);

                        if (usuario.Estado == "true")
                        {
                            usuario.Estado = "Activo";
                        }
                        else
                        {
                            usuario.Estado = "Inactivo";
                        }
                        
                        //se crea el codigo de activacion
                        usuario.CodigoActivacion = Guid.NewGuid();

                        usuario.VerificacionEmail = false;

                        usuario.Valediasp = usuario.Logueo;

                        usuario.Estado = "Registro";

                        db.Entry(usuario).State = EntityState.Modified;

                        db.SaveChanges();

                        var mensaje = "2";

                        //se envia el email al usuario
                        SendVerificationLinkEmail(mensaje,usuario.Contraseña,usuario.Logueo, usuario.CodigoActivacion.ToString(), "VerificarCuenta");
                    }

                    var userASProl = userManager.FindByName(usuario.Logueo);

                    if (usuario.Rol == "true")
                    {
                        userManager.AddToRole(userASProl.Id, "Administrador");
                    }
                    else
                    {
                        userManager.AddToRole(userASProl.Id, "Usuario");
                    }

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado edito la informacion del usuario";
                        eventtemp.IdRegModificado = usuario.Usuarioid;

                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                    
                    //si la edicion fue exitosa se envia el para metro para el mensaje
                    if (valeditcorreomensaje != "verdadero")
                    {
                        return RedirectToAction("Index", new { id = 1 });
                    }
                    else
                    {
                        return RedirectToAction("Index", new { id = 3 });
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ModelState.AddModelError(string.Empty, "Ya existe un usuario con ese email o documento");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }
                }
            }

            ViewBag.LaboratorioId = new SelectList(
                CombosHelper.GetLaboratorios(),
                "LaboratorioId",
                "Nombre", usuario.LaboratorioId);

            ViewBag.SedeId = new SelectList(
              CombosHelper.GetSedes(),
              "SedeId",
              "Nombre", usuario.SedeId);

            return View(usuario);
        }


        // GET: Usuarios/Delete
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Usuario usuario = db.Usuarios.Find(id);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            return View(usuario);
        }


        // POST: Usuarios/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);

            db.Usuarios.Remove(usuario);

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
