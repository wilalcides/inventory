﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Models;

namespace Inventory.Controllers
{

    [Authorize(Roles = "Administrador")]
    public class SedesController : Controller
    {
        private InventoryContext db = new InventoryContext();

        
        public ActionResult Index(int? id)
        {            
            //se hace la validacion del parametro para determinar si se muestra el mensaje de modificacion de estado o edicion de registro exitoso
            if (id == 1)
            {
                ViewBag.Message = "Sede actualizada con exito";
            }

            if (id == 2)
            {
                ViewBag.Message = "Estado modificado con exito";
            }

            return View(db.Sedes.ToList());
        }


        // GET: Sedes/Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // consulta la informacion de la sede en la base de datos, para mostrar en la vista
            Sede sede = db.Sedes.Find(id);

            if (sede == null)
            {
                return HttpNotFound();
            }

            return View(sede);
        }


        // GET: Crear sede
        public ActionResult Create(int? id)
        {
            // se marca el estado true para que al crear un registro este quede con estado activo
            Sede sede = new Sede
            {
                Estado = "true"
            };
            
            //se hace la validacion del parametro para determinar si se muestra el mensaje de creacion de registro exitoso
            if (id != null)
            {
                ViewBag.Message = "Sede registrada con exito";                                                          
            }

            return View(sede);
        }


        // Post crear sede        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Sede sede)
        {            
            if (ModelState.IsValid)
            {                
                db.Sedes.Add(sede);

                //se realiza el cambio del estado de boleano a strins para guardar en la base de datos y mostrarlo en la vista index
                if (sede.Estado == "true")
                {
                    sede.Estado = "Activa";
                }
                else
                {
                    sede.Estado = "Inactiva";
                }
                
                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        
                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado realizo el registro de una Sede.";
                        eventtemp.IdRegModificado = sede.SedeId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                   
                    // si el registro fue exitoso se envia el parametro id con valor 1 para mostrar mensaje
                    return RedirectToAction("Create", "Sedes", new { id = 1 });                    
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "La sede ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                    
                }
            }

            return View(sede);
        }

       
        // GET: Activar o desactivar sede
        public ActionResult Activar_Desactivar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Sede sede = db.Sedes.Find(id);
            
            if (sede == null)
            {
                return HttpNotFound();
            }

            //se realiza el cambio del estado para mostrarlo en la vista, la vista pide el dato en boleano
            if (sede.Estado == "Activa")
            {
                sede.Estado = "true";
            }
            else
            {
                sede.Estado = "false";
            }

            return View(sede);
        }


        // Post activar o desactivar sede
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activar_Desactivar(Sede sede)
        {
            if (ModelState.IsValid)
            {
                //se realiza el cambio del estado de boleano a string para guardar la informacion en la base de datos 
                if (sede.Estado == "true")
                {
                    sede.Estado = "Activa";
                }
                else
                {
                    sede.Estado = "Inactiva";
                }
                
                db.Entry(sede).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        var esttmp = sede.Estado;
                        
                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado modifico el estado del Laboratorio y quedo con estado " + esttmp + " . ";
                        eventtemp.IdRegModificado = sede.SedeId;

                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                   
                    return RedirectToAction("Index", new { id = 2 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "La sede ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                    
                }
            }

            return View(sede);
        }
      

        // GET: Editar sede
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // consulta la informacion de la sede en la base de datos, para mostrar en la vista
            Sede sede = db.Sedes.Find(id);

            if (sede == null)
            {
                return HttpNotFound();
            }

            //se realiza el cambio del estado para mostrarlo en la vista, la vista pide el dato en boleano
            if (sede.Estado == "Activa")
            {
                sede.Estado = "true";
            }
            else
            {
                sede.Estado = "false";
            }

            return View(sede);
        }


        // POST: Editar sede       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Sede sede)
        {
            if (ModelState.IsValid)
            {
                //se realiza el cambio del estado de boleano a string para guardar la informacion en la base de datos 
                if (sede.Estado == "true")
                {
                    sede.Estado = "Activa";
                }
                else
                {
                    sede.Estado = "Inactiva";
                }

                // crea objetos para validar si el registro se encuentra relacionado y evitar la edicion del mismo
                var centemp = db.Centrodecostos.Where(s => s.SedeId.Equals(sede.SedeId)).FirstOrDefault();
                var labtemp = db.Laboratorios.Where(s => s.SedeId.Equals(sede.SedeId)).FirstOrDefault();
                
                if (centemp == null && labtemp == null)
                {
                    db.Entry(sede).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();

                        //Se realiza el registro del evento para temas de seguridad y trazabilidad
                        var autentic = User.Identity.IsAuthenticated;
                        var eventtemp = new Eventos();

                        if (autentic)
                        {
                            var nametemp = User.Identity.Name;
                            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                            eventtemp.Fechcha = System.DateTime.Now;
                            eventtemp.UsuarioId = usutemp.Usuarioid;
                            eventtemp.Descripcion = "El usuario logueado edito la informacion de la Sede";
                            eventtemp.IdRegModificado = sede.SedeId;

                            db.Eventos.Add(eventtemp);
                            db.SaveChanges();
                        }
                        
                        return RedirectToAction("Index", new { id = 1 });
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null &&
                        ex.InnerException.InnerException != null &&
                        ex.InnerException.InnerException.Message.Contains("_Index"))
                        {
                            ViewBag.Message = "La sede ya existe";
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, ex.Message);
                        }                       
                    }
                }
                else
                {
                    ViewBag.Message = "El registro ya tiene relaciones con otros registros y no puede ser editado";
                }
            }

            return View(sede);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
