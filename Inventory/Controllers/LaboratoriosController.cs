﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Inventory.Models;


namespace Inventory.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class LaboratoriosController : Controller
    {
        private InventoryContext db = new InventoryContext();
        

        public JsonResult GetCencost(int sedeid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var Centrocostos = db.Centrodecostos.Where(c => c.SedeId == sedeid && c.Estado == "Activo");

            return Json(Centrocostos);
        }


        // GET: Laboratorios
        public ActionResult Index(int? id)
        {
            if (id == 1)
            {
                ViewBag.Message = "Laboratorio actualizado con exito";
            }

            if (id == 2)
            {
                ViewBag.Message = "Estado modificado con exito";
            }
            var laboratorios = db.Laboratorios.Include(l => l.Sede);

            return View(laboratorios.ToList());
        }

               
        // GET: Laboratorios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Laboratorio laboratorio = db.Laboratorios.Find(id);

            if (laboratorio == null)
            {
                return HttpNotFound();
            }

            return View(laboratorio);
        }


        // GET: Laboratorios/Create
        public ActionResult Create(int? id)
        {
            Laboratorio labora = new Laboratorio
            {
                Estado = "true"
            };
            
            if (id != null)
            {
                ViewBag.Message = "Laboratorio registrado con exito";
            }

            ViewBag.SedeId = new SelectList(
				CombosHelper.GetSedes(),		
				"SedeId",
				"Nombre");

            ViewBag.CentrodecostoId = new SelectList(
                CombosHelper.GetCentrosdecostos(),
                "CentrodecostoId",
                "Nombre");

            return View(labora);
        }


        // POST: Laboratorios/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Laboratorio laboratorio)
        {
            if (ModelState.IsValid)
            {
                db.Laboratorios.Add(laboratorio);

                if (laboratorio.Estado == "true")
                {
                    laboratorio.Estado = "Activo";
                }
                else
                {
                    laboratorio.Estado = "Inactivo";
                }

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                       
                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado realizo el registro de un laboratorio.";
                        eventtemp.IdRegModificado = laboratorio.LaboratorioId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();

                    }
                   
                    ViewBag.SedeId = new SelectList(
                      CombosHelper.GetSedes(),
                      "SedeId",
                      "Nombre", laboratorio.SedeId);

                    ViewBag.CentrodecostoId = new SelectList(
                        CombosHelper.GetCentrosdecostos(),
                        "CentrodecostoId",
                        "Nombre", laboratorio.CentrodecostoId);

                    return RedirectToAction("Create", "Laboratorios", new { id = 1 });              
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {                
                        ViewBag.Message = "El laboratorio ya existe";                        
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);                       
                    }                   
                }
            }          

            ViewBag.SedeId = new SelectList(
				CombosHelper.GetSedes(),
				"SedeId",
				"Nombre", laboratorio.SedeId);

            ViewBag.CentrodecostoId = new SelectList(
                CombosHelper.GetCentrosdecostos(),
                "CentrodecostoId",
                "Nombre", laboratorio.CentrodecostoId);
            
            return View(laboratorio);
        }
        
        
        // GET: Sedes/Edit/5
        public ActionResult Activar_Desactivar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Laboratorio laboratorio = db.Laboratorios.Find(id);

            if (laboratorio == null)
            {
                return HttpNotFound();
            }

            if (laboratorio.Estado == "Activo")
            {
                laboratorio.Estado = "true";
            }
            else
            {
                laboratorio.Estado = "false";
            }

            return View(laboratorio);
        }


        // POST: Sedes/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activar_Desactivar(Laboratorio laboratorio)
        {
            if (ModelState.IsValid)
            {                
                if (laboratorio.Estado == "true")
                {
                    laboratorio.Estado = "Activo";
                }
                else
                {
                    laboratorio.Estado = "Inactivo";
                }

                db.Entry(laboratorio).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        var esttmp = laboratorio.Estado;

                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado modifico el estado del Laboratorio y quedo con estado " + esttmp + " . ";
                        eventtemp.IdRegModificado = laboratorio.LaboratorioId;

                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }
                    
                    return RedirectToAction("Index", new { id = 2 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "El laboratorio ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                   
                }

            }

            return View(laboratorio);
        }


        // GET: Laboratorios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Laboratorio laboratorio = db.Laboratorios.Find(id);

            if (laboratorio == null)
            {
                return HttpNotFound();
            }

            if (laboratorio.Estado == "Activo")
            {
                laboratorio.Estado = "true";
            }
            else
            {
                laboratorio.Estado = "False";
            }

            ViewBag.SedeId = new SelectList(
				CombosHelper.GetSedesedit(),
				"SedeId",
				"Nombre", laboratorio.SedeId);

            ViewBag.CentrodecostoId = new SelectList(
                CombosHelper.GetCentrosdecostosedit(laboratorio.SedeId),
                "CentrodecostoId",
                "Nombre", laboratorio.CentrodecostoId);

            return View(laboratorio);
        }


        // POST: Laboratorios/Edit/5       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Laboratorio laboratorio)
        {
            if (ModelState.IsValid)
            {
                if (laboratorio.Estado == "true")
                {
                    laboratorio.Estado = "Activo";
                }
                else
                {
                    laboratorio.Estado = "Inactivo";
                }

                var centemp = db.Dispositivos.Where(s => s.LaboratorioId.Equals(laboratorio.LaboratorioId)).FirstOrDefault();
                var usuatemp = db.Usuarios.Where(s => s.LaboratorioId.Equals(laboratorio.LaboratorioId)).FirstOrDefault();

                if (centemp == null || usuatemp == null)
                {
                    db.Entry(laboratorio).State = EntityState.Modified;
                    try
                    {
                        db.SaveChanges();

                        //Se realiza el registro del evento para temas de seguridad y trazabilidad
                        var autentic = User.Identity.IsAuthenticated;
                        var eventtemp = new Eventos();

                        if (autentic)
                        {
                            var nametemp = User.Identity.Name;
                            var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();

                            eventtemp.Fechcha = System.DateTime.Now;
                            eventtemp.UsuarioId = usutemp.Usuarioid;
                            eventtemp.Descripcion = "El usuario logueado edito la informacion del usuario";
                            eventtemp.IdRegModificado = laboratorio.LaboratorioId;

                            db.Eventos.Add(eventtemp);
                            db.SaveChanges();
                        }
                       
                        return RedirectToAction("Index", new { id = 1 });
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null &&
                        ex.InnerException.InnerException != null &&
                        ex.InnerException.InnerException.Message.Contains("_Index"))
                        {
                            ViewBag.Message = "El laboratorio ya existe";
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, ex.Message);
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "El registro ya tiene relaciones con otros registros y no puede ser editado";
                }
            }          
        
            ViewBag.SedeId = new SelectList(
				CombosHelper.GetSedesedit(),		
				"SedeId",
				"Nombre", laboratorio.SedeId);

            ViewBag.CentrodecostoId = new SelectList(
                 CombosHelper.GetCentrosdecostos(),
                 "CentrodecostoId",
                 "Nombre", laboratorio.CentrodecostoId);

            return View(laboratorio);
        }
                      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
