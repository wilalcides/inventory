﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventory.Models;

namespace Inventory.Controllers
{

    [Authorize(Roles = "Administrador")]
    public class CentrodecostosController : Controller
    {
        private InventoryContext db = new InventoryContext();

        // GET: Centrodecostos
        public ActionResult Index(int? id)
		{
            if (id == 1)
            {

                ViewBag.Message = "Centro de costos actualizado con exito";
            }

            if (id == 2)
            {
                ViewBag.Message = "Estado modificado con exito";
            }

            var Centrodecostos = db.Centrodecostos.Include(c => c.Sede);

            return View(Centrodecostos.ToList());
        }


        // GET: Centrodecostos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Centrodecosto centrodecosto = db.Centrodecostos.Find(id);

            if (centrodecosto == null)
            {
                return HttpNotFound();
            }

            return View(centrodecosto);
        }


        // GET: Centrodecostos/Create
        public ActionResult Create(int? id)
        {
            Centrodecosto cencost = new Centrodecosto
            {
                Estado = "true"
            };

            if (id != null)
            {
                ViewBag.Message = "Centro de costos registrado con exito";
            }

            ViewBag.SedeId = new SelectList(
                CombosHelper.GetSedes(),
                "SedeId",
                "Nombre");
            
            return View(cencost);
        }


        // POST: Centrodecostos/Create        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Centrodecosto centrodecosto)
        {
            if (ModelState.IsValid)
            {
                db.Centrodecostos.Add(centrodecosto);

                if (centrodecosto.Estado == "true")
                {
                    centrodecosto.Estado = "Activo";
                }
                else
                {
                    centrodecosto.Estado = "Inactivo";
                }

                try
                {
                    db.SaveChanges();

                    //Se realiza el registro del evento para temas de seguridad y trazabilidad
                    var autentic = User.Identity.IsAuthenticated;
                    var eventtemp = new Eventos();

                    if (autentic)
                    {
                        var nametemp = User.Identity.Name;
                        var usutemp = db.Usuarios.Where(s => s.Logueo.Equals(nametemp)).FirstOrDefault();
                        
                        eventtemp.Fechcha = System.DateTime.Now;
                        eventtemp.UsuarioId = usutemp.Usuarioid;
                        eventtemp.Descripcion = "El usuario logueado realizo el registro de un centro de costos.";
                        eventtemp.IdRegModificado = centrodecosto.CentrodecostoId;
                        db.Eventos.Add(eventtemp);
                        db.SaveChanges();
                    }  
                   
                    ViewBag.SedeId = new SelectList(
                    CombosHelper.GetSedes(),
                      "SedeId",
                      "Nombre", centrodecosto.SedeId);
                    
                    return RedirectToAction("Create", "Centrodecostos", new { id = 1 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "El centro de costos ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                    
                }
            }


			ViewBag.SedeId = new SelectList(
			CombosHelper.GetSedes(),
			    "SedeId",
			    "Nombre", centrodecosto.SedeId);

			return View(centrodecosto);
        }


        // GET: Cambio de estado centro de costos 
        public ActionResult Activar_Desactivar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Centrodecosto centrodecosto = db.Centrodecostos.Find(id);

            if (centrodecosto == null)
            {
                return HttpNotFound();
            }

            if (centrodecosto.Estado == "Activo")
            {
                centrodecosto.Estado = "true";
            }
            else
            {
                centrodecosto.Estado = "false";
            }
            return View(centrodecosto);
        }


        // POST: Cambio de estado centro de costos       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activar_Desactivar(Centrodecosto centrodecosto)
        {
            if (ModelState.IsValid)
            {
                if (centrodecosto.Estado == "true")
                {
                    centrodecosto.Estado = "Activo";
                }
                else
                {
                    centrodecosto.Estado = "Inactivo";
                }

                db.Entry(centrodecosto).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    return RedirectToAction("Index", new { id = 2 });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                    {
                        ViewBag.Message = "El centro de costos ya existe";
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }                    
                }
            }

            return View(centrodecosto);
        }

        
        // GET: Centrodecostos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Centrodecosto centrodecosto = db.Centrodecostos.Find(id);

            if (centrodecosto == null)
            {
                return HttpNotFound();
            }

            if (centrodecosto.Estado == "Activo")
            {
                centrodecosto.Estado = "true";
            }
            else
            {
                centrodecosto.Estado = "false";
            }

            ViewBag.SedeId = new SelectList(
				CombosHelper.GetSedesedit(),
				"SedeId",
				"Nombre", centrodecosto.SedeId);
            
			return View(centrodecosto);
        }


        // POST: Centrodecostos/Edit/5       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Centrodecosto centrodecosto)
        {
            if (ModelState.IsValid)
            {
                if (centrodecosto.Estado == "true")
                {
                    centrodecosto.Estado = "Activo";
                }
                else
                {
                    centrodecosto.Estado = "Inactivo";
                }
                
                var labtemp = db.Laboratorios.Where(s => s.CentrodecostoId.Equals(centrodecosto.CentrodecostoId)).FirstOrDefault();

                if (labtemp == null)
                {
                    db.Entry(centrodecosto).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("Index", new { id = 1 });
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null &&
                        ex.InnerException.InnerException != null &&
                        ex.InnerException.InnerException.Message.Contains("_Index"))
                        {
                                ViewBag.Message = "El centro de costos ya existe";
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, ex.Message);
                        }
                    
                    }
                }
                else
                {
                    ViewBag.Message = "El registro ya tiene relaciones con otros registros y no puede ser editado";
                }
            }

			ViewBag.SedeId = new SelectList(
			 CombosHelper.GetSedesedit(),
				"SedeId",
				"Nombre", centrodecosto.SedeId);

			return View(centrodecosto);
        }


        // GET: Centrodecostos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Centrodecosto centrodecosto = db.Centrodecostos.Find(id);

            if (centrodecosto == null)
            {
                return HttpNotFound();
            }

            return View(centrodecosto);
        }


        // POST: Centrodecostos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Centrodecosto centrodecosto = db.Centrodecostos.Find(id);

            db.Centrodecostos.Remove(centrodecosto);
           
			try
			{
				db.SaveChanges();

				return RedirectToAction("Index");
			}
			catch (Exception ex)
			{
				if (ex.InnerException != null &&
					ex.InnerException.InnerException != null &&
					ex.InnerException.InnerException.Message.Contains("REFERENCE"))
				{
                    ViewBag.Message = "el registro no puede ser eliminado por que tiene registros relacionados";
				}
				else
				{
					ModelState.AddModelError(string.Empty, ex.Message);
				}
			}

			return View(centrodecosto);
		}


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
