﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Inventory.Clases
{
    public class MetodosHelper : IDisposable
    {
        private static InventoryContext db = new InventoryContext();


        //Metod para cargar la iagen en el servidor
        public static string SubirfFirma(HttpPostedFileBase imagen, string ruta)
        {
            ///Upload Photo
            string path = string.Empty;
            string pic = string.Empty;

            if (imagen != null)
            {
                pic = Path.GetFileName(imagen.FileName);
                path = Path.Combine(HttpContext.Current.Server.MapPath(ruta), pic);
                imagen.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    imagen.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

            return pic;
        }



        //metodo para encriptar las contraseñas
        public static string Hash(string value)
        {
            return Convert.ToBase64String(
                System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(value))
                                          );
        }


        //Metodo para limpiar de la base de datos la validacion realizadada por un usuario especifoco
        public static void EliminarValDetaTmp(int? id)
        {
            foreach (var disp in db.ValidadorDetalleTmps.Where(u => u.UsuarioId == id))
            {
                db.ValidadorDetalleTmps.Remove(disp);
            }

            db.SaveChanges();
        }


        //Metodo para limpiar de la base de datos la informacion del reporte de la validacion con la que se realiza la exportacion a un pdf un usuario especifico
        public static void EliminarRepVal(int? id)
        {
            foreach (var disp in db.ReporteValidaciones.Where(u => u.UsuarioId == id))
            {
                db.ReporteValidaciones.Remove(disp);
            }

            db.SaveChanges();
        }


        //Metodo para limpiar el detalle del validar cuando ya se realiza la exportacion del reporte de validacion 
        public static void EliminarValDet(int? id)
        {
            foreach (var disp in db.ValidadorDetalles.Where(u => u.UsuarioId == id))
            {
                db.ValidadorDetalles.Remove(disp);
            }

            db.SaveChanges();
        }


        // Metodo para eliminar los registros del reporte cuando se realiza la exportacion
        public static void EliminaReporte(int? id)
        {
            foreach (var disp in db.Reportes.Where(u => u.UsuarioId == id))
            {
                db.Reportes.Remove(disp);
            }

            db.SaveChanges();
        }



        public void Dispose()
        {

            db.Dispose();

        }


    }
}