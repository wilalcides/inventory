﻿using Inventory.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
	public class CombosHelper : IDisposable
	{

		private static InventoryContext db = new InventoryContext();


        //Metodo para mostrar listado del tipo de histoicos
        public static List<Tipo_Historico> GetTipo()
        {            
            var Tipos = db.Tipo_Historicos.ToList();
            Tipos.Add(new Tipo_Historico
            {
                TipoId = 0,
                Nombre = " [Seleccione el tipo]",
            }
                      );

            return Tipos.OrderBy(d => d.Nombre).ToList();
        }


        // Metodo que muestra lista de dispositivos
        public static List<Dispositivo> GetDispositivos()
		{
			var dispositivos = db.Dispositivos.ToList();
			dispositivos.Add(new Dispositivo
			{
				DispositivoId = 0,
				Nombre = " [Seleccione un Dispositivo...]",
			}
				            );

			return dispositivos.OrderBy(d => d.Nombre).ToList();
		}


        //Metodo que muestra una lista de usuarios que esten activos en el sistema
		public static List<Usuario> GetUsuarios()

		{
			var usuarios = db.Usuarios.Where(u => u.Estado.ToLower().Equals("Activo")).ToList();
			usuarios.Add(new Usuario
			{
				Usuarioid = 0,
				Nombre = " [Seleccione un Usuario...]",
			}
				        );

			return usuarios.OrderBy(d => d.Nombre).ToList();
		}


        //Metodo que muestra lista de sedes activas en el sistema
		public static	List<Sede> GetSedes()			
		{                       
            var sedes = db.Sedes.Where(u => u.Estado.ToLower().Equals("Activa")). ToList();
			sedes.Add(new Sede
			{
			SedeId =0,
			Nombre = " [Seleccione una Sede...]",

			}
			      	);

			return sedes.OrderBy(d => d.Nombre).ToList();
		}


        //Metodo que muestra lista de estados para los dispositivos
		public static List<Estado> GetEstados()
        {
            var estados = db.Estados.ToList();
            estados.Add(new Estado
            {
                EstadoId = 0,
                Nombre = " [Seleccione un Estado...]",

            }
                        );

            return estados.OrderBy(d => d.Nombre).ToList();
        }


        //Metodo que muestra listado de laboratorios activos en el sistema
        public static List<Laboratorio> GetLaboratorios()
        {
            var laboratorios = db.Laboratorios.Where(u => u.Estado.ToLower().Equals("Activo")).ToList();
            laboratorios.Add(new Laboratorio
            {
                LaboratorioId = 0,
                Nombre = " [Seleccione un Laboratorio...]",
            }
                );

            return laboratorios.OrderBy(d => d.Nombre).ToList();
        }


        //Metodo que muestra listado de laboratorios de una sede especifica 
		public static List<Laboratorio> GetLaboratoriosed(int? id)
		{
			var laboratorios = db.Laboratorios.Where(u => u.SedeId == id && u.Estado == "Activo").ToList();
			laboratorios.Add(new Laboratorio
			{
				LaboratorioId = 0,
				Nombre = " [Seleccione un Laboratorio...]",
			}
				);

			return laboratorios.OrderBy(d => d.Nombre).ToList();
		}


        //Metodo que muestra los centro de costos activos en el sistema
		public static List<Centrodecosto> GetCentrosdecostos()
        {
            var Centrosdecostos = db.Centrodecostos.Where(u => u.Estado.ToLower().Equals("Activo")).ToList();
            Centrosdecostos.Add(new Centrodecosto
            {
                CentrodecostoId = 0,
                Nombre = " [Seleccione un Centro de costos...]",
            }
                                );

            return Centrosdecostos.OrderBy(d => d.Nombre).ToList();
        }


        // Metodo utilizado en las vistas edit para mostrar una lista de centros de costos
        public static List<Centrodecosto> GetCentrosdecostosedit(int? id)
        {
            
            var Centrosdecostos = db.Centrodecostos.Where(u => u.SedeId == id && u.Estado == "Activo").ToList();
            Centrosdecostos.Add(new Centrodecosto
            {
                CentrodecostoId = 0,
                Nombre = " [Seleccione un Centro de costos...]",
            }
                                );

            return Centrosdecostos.OrderBy(d => d.Nombre).ToList();
        }


        // Metodo utilizado en las vistas edit para mostrar una lista de sedes
        public static List<Sede> GetSedesedit()
        {
            var sedes = db.Sedes.ToList();
            sedes.Add(new Sede
            {
                SedeId = 0,
                Nombre = " [Seleccione una Sede...]",
            }
                      );

            return sedes.OrderBy(d => d.Nombre).ToList();
        }


        public void Dispose()
		{
			db.Dispose();
		}
	}
}